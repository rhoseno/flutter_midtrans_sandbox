#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#
Pod::Spec.new do |s|
  s.name             = 'flutter_midtrans_sandbox'
  s.version          = '0.0.1'
  s.summary          = 'Flutter Sandbox Midtrans Payment Gateway'
  s.description      = <<-DESC
Flutter Sandbox Midtrans Payment Gateway
                       DESC
  s.homepage         = 'https://www.rentfix.com'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Rentfix.com' => 'rhoseno@rentfix.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.public_header_files = 'Classes/**/*.h'
  s.dependency 'Flutter'
  s.dependency 'MidtransCoreKit'
  s.dependency 'MidtransKit'
  s.ios.deployment_target = '8.0'
end


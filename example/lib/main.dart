import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_midtrans_sandbox/flutter_midtrans_sandbox.dart';

void main() => runApp(new MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool isMakePayment = false;
  final midtrans = FlutterMidtransSandbox();
  @override
  void initState() {
    super.initState();
    midtrans.init("YOUR_CLIENT_ID", "YOUR_URL_BASE");
    midtrans.setFinishCallback(_callback);
  }

  _makePayment() {
    setState(() {
      isMakePayment = true;
    });
    midtrans
        .makePayment(
      MidtransTransaction(
          3200000,
          MidtransCustomer(
              "Didi", "Rhoseno Ardi", "rhoseno98@gmail.com", "082122628744"),
          [
            MidtransItem(
              "RENT-732402150001",
              3200000,
              11,
              "Gudang Di Daerah XXX",
            )
          ],
          skipCustomer: true,
          customField1: "ANYCUSTOMFIELD"),
    )
        .catchError((err) => print("ERROR $err"));
  }

  Future<void> _callback(TransactionFinished finished) async {
    setState(() {
      isMakePayment = false;
    });
    return Future.value(null);
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        appBar: new AppBar(
          title: const Text('Sandbox'),
        ),
        body: new Center(
          child: isMakePayment
              ? CircularProgressIndicator()
              : RaisedButton(
            child: Text("Pay Now"),
            onPressed: () => _makePayment(),
          ),
        ),
      ),
    );
  }
}
